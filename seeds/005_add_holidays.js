const data = [
  {
    date_off: '2020-03-22',
    name: 'Isra Miraj'
  },
  {
    date_off: '2020-03-25',
    name: 'Hari Raya Nyepi'
  },
  {
    date_off: '2020-04-10',
    name: 'Jumat Agung'
  },
  {
    date_off: '2020-05-01',
    name: 'Hari Buruh'
  },
  {
    date_off: '2020-05-07',
    name: 'Hari Raya Waisak'
  },
  {
    date_off: '2020-05-21',
    name: 'Kenaikan Isa Almasih'
  },
  {
    date_off: '2020-05-24',
    name: 'Idul Fitri'
  },
  {
    date_off: '2020-05-25',
    name: 'Idul Fitri'
  }
];

exports.seed = knex =>
  knex('holidays')
    .del()
    .then(() => knex('holidays').insert(data));
