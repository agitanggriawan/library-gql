const data = [
  {
    grade: '7'
  },
  {
    grade: '8'
  },
  {
    grade: '9'
  }
];

exports.seed = knex =>
  knex('classes')
    .del()
    .then(() => knex('classes').insert(data));
