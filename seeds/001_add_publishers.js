const data = [
  {
    name: 'Bukune'
  },
  {
    name: 'Falcon Publishing'
  },
  {
    name: 'Jaring Pena'
  },
  {
    name: 'Bentang'
  },
  {
    name: 'Gramedia Pustaka'
  },
  {
    name: 'Mizan Pustaka'
  },
  {
    name: 'Expose'
  },
  {
    name: 'Gagas Media'
  },
  {
    name: 'Sabil'
  },
  {
    name: 'Dian Rakyat'
  },
  {
    name: 'DAR! Mizan'
  },
  {
    name: 'Elex Media Komputindo'
  },
  {
    name: 'Pustaka Insan Madani'
  },
  {
    name: 'Gading Inti Prima'
  },
  {
    name: 'IPA Abong'
  },
  {
    name: 'Kaifa'
  },
  {
    name: 'Global Publisher'
  },
  {
    name: 'Republika'
  },
  {
    name: 'Aneka Ilmu'
  },
  {
    name: 'Lingkar Pena Kr'
  },
  {
    name: 'Ciptamedia Binanusa'
  },
  {
    name: 'Anugerah'
  },
  {
    name: 'Ghyyas Putra'
  },
  {
    name: 'Apollo Lestari'
  },
  {
    name: 'Cakra Media'
  },
  {
    name: 'Intan Pariwara'
  },
  {
    name: 'Chandra Pratama'
  },
  {
    name: 'Andhika Prawira'
  }
];

exports.seed = knex =>
  knex('publishers')
    .del()
    .then(() => knex('publishers').insert(data));
