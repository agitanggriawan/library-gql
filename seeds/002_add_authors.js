const data = [
  {
    name: 'Boy Chandra'
  },
  {
    name: 'Agnes Davonar'
  },
  {
    name: 'Saroyan Royan'
  },
  {
    name: 'Dewi Lestari'
  },
  {
    name: 'Erryga Yogasmara'
  },
  {
    name: 'Tere Liye'
  },
  {
    name: 'Jostein Gearder'
  },
  {
    name: 'Akmal Nasery Basra'
  },
  {
    name: 'Diego Christian'
  },
  {
    name: 'Prahara Putra'
  },
  {
    name: 'Tony Buzan'
  },
  {
    name: 'K. B. Ono'
  },
  {
    name: 'Sari Pusparini Saleh'
  },
  {
    name: 'Thomas Soetikno'
  },
  {
    name: 'Hartadi'
  },
  {
    name: 'Agus HK Soetomo'
  },
  {
    name: 'Krido B'
  },
  {
    name: 'S. Artaria'
  },
  {
    name: 'Diana Susyanti'
  },
  {
    name: 'Griselda Al Arifin'
  },
  {
    name: 'Siwi Purwati Syafrizal'
  },
  {
    name: 'Beth Spanjian'
  },
  {
    name: 'Margret'
  },
  {
    name: 'Patricia Kendell'
  },
  {
    name: 'Mahfud Ikhwan'
  },
  {
    name: 'D.H. Sunjaya'
  },
  {
    name: 'Aditya Pradana'
  },
  {
    name: 'Comic Tribe'
  },
  {
    name: 'Seiichi Konishi'
  },
  {
    name: 'Ian Graham'
  },
  {
    name: 'Matthew Wolf'
  },
  {
    name: 'Randi Catono'
  },
  {
    name: 'Fiona Chandler'
  },
  {
    name: 'Dzikry el-Hans'
  },
  {
    name: 'Wahyudin'
  },
  {
    name: 'Sally Grindley'
  },
  {
    name: 'Norah McClintock'
  },
  {
    name: 'Enid Blyton'
  },
  {
    name: 'Adi Wibawa'
  },
  {
    name: 'Ekky Al Malaky'
  },
  {
    name: 'Nawawi Rambe'
  },
  {
    name: 'Dindin Solahudin'
  },
  {
    name: 'Ahmadun'
  },
  {
    name: 'Sukanto'
  },
  {
    name: 'FLP DKI'
  },
  {
    name: 'Ade Fathu Rachim'
  },
  {
    name: 'Purwo Djatmiko'
  },
  {
    name: 'Abi Wisam'
  },
  {
    name: 'Grace Windsor'
  },
  {
    name: 'M. Samsuri'
  },
  {
    name: 'Harlina Pribadi'
  },
  {
    name: 'Ngapiningsih'
  },
  {
    name: 'Liana MP'
  }
];

exports.seed = knex =>
  knex('authors')
    .del()
    .then(() => knex('authors').insert(data));
