const data = [
  {
    name: 'A-101'
  },
  {
    name: 'A-102'
  },
  {
    name: 'B-101'
  },
  {
    name: 'B-102'
  },
  {
    name: 'C-101'
  },
  {
    name: 'C-102'
  }
];

exports.seed = knex =>
  knex('bookshelfs')
    .del()
    .then(() => knex('bookshelfs').insert(data));
