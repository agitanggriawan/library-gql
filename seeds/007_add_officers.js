const bcrypt = require('bcrypt');

const data = [
  {
    role: 'Admin',
    name: 'Zainuddin',
    email: 'zain@dinamika.ac.id',
    address: 'Brongkalan, Surabaya',
    phone: '08123456789',
    password: bcrypt.hashSync('password', bcrypt.genSaltSync(10))
  }
];

exports.seed = knex =>
  knex('officers')
    .del()
    .then(() => knex('officers').insert(data));
