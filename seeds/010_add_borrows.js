const bcrypt = require('bcrypt');

const data = [
  {
    officer_id: 1,
    member_id: 6,
    book_id: 48,
    is_borrowed: false,
    borrow_at: '2020-04-12',
    max_return: '2020-04-15',
    return_at: '2020-04-15',
    penalty: 0,
  },
  {
    officer_id: 1,
    member_id: 27,
    book_id: 53,
    is_borrowed: false,
    borrow_at: '2020-04-12',
    max_return: '2020-04-15',
    return_at: '2020-04-15',
    penalty: 0,
  },
];

exports.seed = (knex) =>
  knex('borrows').then(() => knex('borrows').insert(data));
