module.exports = {
  local: {
    client: 'postgresql',
    connection: {
      database: 'library_local',
      user: 'postgres',
      password: 'password',
      host: 'localhost',
      port: 5432
    },
    pool: {
      min: 10,
      max: 100
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
  production: {
    client: 'postgresql',
    connection: {
      database: 'd3dk5ql60fu18b',
      user: 'nrxkfibqrnlllg',
      password: '04cd1b8d18d6ff52b7d4c6723d7229a8fa271ccee4cd07bf1c0f8e9855683b7c',
      host: 'ec2-3-234-169-147.compute-1.amazonaws.com',
      port: 5432
    },
    pool: {
      min: 10,
      max: 100
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }
};
