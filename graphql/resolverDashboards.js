const { Publisher } = require('../models');

const resolvers = {
  Query: {
    async totalPublisher() {
      console.log('==> Accessing totalPublisher');

      return Publisher.query().count('id').first();
    },
  },
};

module.exports = resolvers;
