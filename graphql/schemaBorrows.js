const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    borrows(class_id: ID): [Borrow]
    borrow(id: ID): Borrow
    returns: [Borrow]
  }

  extend type Mutation {
    createBorrow(data: InBorrow!): Borrow
    updateBorrow(id: ID!): Borrow
    returnBook(id: ID!, penalty: Int!): Borrow
  }

  type Borrow {
    id: ID!
    officer_id: ID!
    member_id: ID!
    book_id: ID!
    is_borrowed: Boolean
    payment_penalty: Int
    borrow_at: Date!
    max_return: Date
    return_at: Date
    created_at: DateTime
    updated_at: DateTime

    officer: Officer
    member: Member
    book: Book
  }

  input InBorrow {
    officer_id: ID!
    member_id: ID!
    book_id: ID!
    borrow_at: Date!
  }
`;

module.exports = typeDefs;
