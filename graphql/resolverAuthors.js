const { Author } = require('../models');

const resolvers = {
  Query: {
    async authors(_, args) {
      console.log('==> Accessing authors');
      return Author.query().orderBy('name', 'asc');
    },
    async author(_, args) {
      console.log('==> Accessing author', args.id);
      return Author.query().findOne({ id: args.id });
    },
    async searchAuthor(_, args) {
      console.log('==> Accessing searchAuthor');
      return Author.query()
        .where('name', 'ilike', `%${args.name}%`)
        .orderBy('name', 'asc');
    }
  },
  Mutation: {
    async createAuthor(_, args) {
      console.log('==> Accessing createAuthor');

      return Author.query()
        .insert(args.data)
        .returning('*');
    },
    async updateAuthor(_, args) {
      console.log('==> Accessing updateAuthor');

      return Author.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    }
  }
};

module.exports = resolvers;
