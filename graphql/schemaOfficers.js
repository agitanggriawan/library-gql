const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    authenticate(email: String!, password: String!): UserAuth!
    officers: [Officer]
    officer(id: ID!): Officer
    searchOfficer(name: String!): [Officer]
  }

  extend type Mutation {
    createOfficer(data: InOfficer!): Officer
    updateOfficer(id: ID!, data: InOfficer!): Officer
  }

  type UserAuth {
    id: String!
    role: String!
    name: String!
    email: String!
    token: String!
  }

  type Officer {
    id: ID!
    name: String!
    role: String!
    email: String!
    address: String
    phone: String
    password: String!
    created_at: DateTime
    updated_at: DateTime
  }

  input InOfficer {
    name: String!
    role: String!
    email: String!
    address: String
    phone: String
    password: String!
  }
`;

module.exports = typeDefs;
