const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    holidays: [Holiday]
    holiday(id: ID!): Holiday
  }

  extend type Mutation {
    createHoliday(data: InHoliday!): Holiday
    updateHoliday(id: ID!, data: InHoliday!): Holiday
  }

  type Holiday {
    id: ID!
    date_off: Date!
    name: String!
    created_at: DateTime!
    updated_at: DateTime!
  }

  input InHoliday {
    date_off: Date!
    name: String!
  }
`;

module.exports = typeDefs;
