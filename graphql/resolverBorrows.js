const { Borrow } = require('../models');

const resolvers = {
  Query: {
    async borrows(_, args) {
      console.log('==> Accessing borrows');
      const { class_id } = args;

      return Borrow.query()
        .where({ is_borrowed: true })
        .orderBy('borrow_at', 'desc')
        .withGraphFetched(
          '[officer, member.class, book.[publisher, author, bookshelf]]'
        )
        .modify(qb => {
          if (class_id) {
            qb.joinRelation('member', { alias: 'm' });
            qb.where('m.class_id', class_id);
          }
        });
    },
    async borrow(_, args) {
      console.log('==> Accessing borrow');
      return Borrow.query()
        .findOne({ id: args.id })
        .withGraphFetched(
          '[officer, member.class, book.[publisher, author, bookshelf]]'
        );
    },
    async returns(_, args) {
      console.log('==> Accessing returns');

      return Borrow.query()
        .where({ is_borrowed: false })
        .orderBy('borrow_at', 'desc')
        .withGraphFetched(
          '[officer, member.class, book.[publisher, author, bookshelf]]'
        );
    }
  },
  Mutation: {
    async createBorrow(_, args) {
      console.log('==> Accessing createBorrow');

      return Borrow.query()
        .insert(args.data)
        .returning('*');
    },
    async updateBorrow(_, args) {
      console.log('==> Accessing updateBorrow');

      return Borrow.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    },
    async returnBook(_, args) {
      console.log('==> Accessing returnBook');

      return Borrow.query()
        .patchAndFetchById(args.id, {
          is_borrowed: false,
          return_at: new Date(),
          penalty: args.penalty
        })
        .returning('*');
    }
  }
};

module.exports = resolvers;
