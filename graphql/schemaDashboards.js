const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    totalPublisher: Total
  }
`;

module.exports = typeDefs;
