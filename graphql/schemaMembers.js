const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    members: [Member]
    member(id: ID!): Member
    searchMember(name: String!): [Member]
  }

  extend type Mutation {
    createMember(data: InMember!): Member
    updateMember(id: ID!, data: InMember!): Member
    verifyMember(registration_token: String): Member
  }

  type Member {
    id: ID!
    class_id: ID!
    name: String!
    email: String
    address: String
    gender: String!
    status: String!
    phone: String
    birthdate: Date
    photo: String
    registration_token: String
    created_at: DateTime
    updated_at: DateTime
    class: Class
  }

  input InMember {
    class_id: ID
    name: String
    email: String
    address: String
    gender: String
    status: String
    phone: String
    birthdate: Date
    photo: Upload
  }
`;

module.exports = typeDefs;
