const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    movies(count: Int): [Movie]
    moviesPaginate(first: Int, offset: Int): [Movie]
    moviesFilter(filter: String): [Movie]
    movie(id: Int!): Movie
    user(id: ID!): User
  }

  extend type Mutation {
    createMovie(data: InMovie): Movie
    createUser(uuid: String, name: String): Boolean
  }

  input InMovie {
    id: Int!
    title: String!
    url: String!
  }

  type Movie {
    id: Int!
    title: String!
    url: String
  }

  type User {
    id: ID!
    name: String!
    role: Roles
  }
`;

module.exports = typeDefs;
