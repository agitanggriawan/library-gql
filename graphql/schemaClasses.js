const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    classes: [Class]
    class(id: ID!): Class
  }

  extend type Mutation {
    createClass(data: InClass!): Class
    updateClass(id: ID!, data: InClass!): Class
  }

  type Class {
    id: ID!
    grade: String!
    created_at: DateTime!
    updated_at: DateTime!
  }

  input InClass {
    grade: String!
  }
`;

module.exports = typeDefs;
