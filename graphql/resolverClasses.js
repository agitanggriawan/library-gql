const { Class } = require('../models');

const resolvers = {
  Query: {
    async classes(_, args) {
      console.log('==> Accessing classes');
      return Class.query().orderBy('grade', 'asc');
    },
    async class(_, args) {
      console.log('==> Accessing class', args.id);
      return Class.query().findOne({ id: args.id });
    }
  },
  Mutation: {
    async createClass(_, args) {
      console.log('==> Accessing createClass');

      return Class.query()
        .insert(args.data)
        .returning('*');
    },
    async updateClass(_, args) {
      console.log('==> Accessing updateClass');

      return Class.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    }
  }
};

module.exports = resolvers;
