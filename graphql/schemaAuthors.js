const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    authors: [Author]
    author(id: ID!): Author
    searchAuthor(name: String!): [Author]
  }

  extend type Mutation {
    createAuthor(data: InAuthor!): Author
    updateAuthor(id: ID!, data: InAuthor!): Author
  }

  type Author {
    id: ID!
    name: String!
    created_at: DateTime!
    updated_at: DateTime!
  }

  input InAuthor {
    name: String!
  }
`;

module.exports = typeDefs;
