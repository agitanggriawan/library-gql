const fs = require('fs');
const { Book } = require('../models');
const { formatRegistartionNumber } = require('../utils');

const resolvers = {
  Query: {
    async books(_, args) {
      console.log('==> Accessing books');
      return Book.query()
        .orderBy('registration_number', 'asc')
        .withGraphFetched('[publisher, author, bookshelf]');
    },
    async book(_, args) {
      console.log('==> Accessing book', args.id);
      return Book.query()
        .findOne({ id: args.id })
        .withGraphFetched('[publisher, author, bookshelf]');
    },
    async searchBook(_, args) {
      console.log('==> Accessing searchBook');
      return Book.query()
        .where('title', 'ilike', `%${args.title}%`)
        .orWhere('registration_number', 'ilike', `%${args.title}%`)
        .andWhere('quantity', '>', 0)
        .orderBy('title', 'asc');
    }
  },
  Mutation: {
    async createBook(_, args) {
      console.log('==> Accessing createBook');
      let url = null;
      if (args.data.cover_url && args.data.cover_url.length) {
        url = await args.data.cover_url[0].then(file => {
          const { createReadStream, filename } = file;
          const fileStream = createReadStream();
          const now = Date.now();
          const path = `${now}-${filename}`;

          fileStream.pipe(fs.createWriteStream(`./public/${path}`));
          return path;
        });
      }
      args.data.cover_url = url;
      args.data.registration_number =
        parseInt(args.data.registration_number, 10) - 1;
      const books = Array(args.data.quantity).fill(args.data);
      const formattedBooks = books.map((e, i) => ({
        ...e,
        registration_number: `${formatRegistartionNumber(
          parseInt(e.registration_number, 10) + (i + 1)
        )}/SMP PGRI 6/${new Date().getFullYear()}`
      }));

      return Book.query()
        .insert(formattedBooks)
        .returning('*')
        .first();
    },
    async updateBook(_, args) {
      console.log('==> Accessing updateBook');

      return Book.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    }
  }
};

module.exports = resolvers;
