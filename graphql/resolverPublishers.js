const { Publisher } = require('../models');

const resolvers = {
  Query: {
    async publishers(_, args) {
      console.log('==> Accessing publishers');
      return Publisher.query().orderBy('name', 'asc');
    },
    async publisher(_, args) {
      console.log('==> Accessing publisher');
      return Publisher.query().findOne({ id: args.id });
    },
    async searchPublisher(_, args) {
      console.log('==> Accessing searchPublisher');
      return Publisher.query()
        .where('name', 'ilike', `%${args.name}%`)
        .orderBy('name', 'asc');
    }
  },
  Mutation: {
    async createPublisher(_, args) {
      console.log('==> Accessing createPublisher');

      return Publisher.query()
        .insert(args.data)
        .returning('*');
    },
    async updatePublisher(_, args) {
      console.log('==> Accessing updatePublisher');

      return Publisher.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    }
  }
};

module.exports = resolvers;
