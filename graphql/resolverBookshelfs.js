const { Bookshelf } = require('../models');

const resolvers = {
  Query: {
    async bookshelfs(_, args) {
      console.log('==> Accessing bookshelfs');
      return Bookshelf.query().orderBy('name', 'asc');
    },
    async bookshelf(_, args) {
      console.log('==> Accessing bookshelf', args.id);
      return Bookshelf.query().findOne({ id: args.id });
    },
    async searchBookshelf(_, args) {
      console.log('==> Accessing searchBookshelf');
      return Bookshelf.query()
        .where('name', 'ilike', `%${args.name}%`)
        .orderBy('name', 'asc');
    }
  },
  Mutation: {
    async createBookshelf(_, args) {
      console.log('==> Accessing createBookshelf');

      return Bookshelf.query()
        .insert(args.data)
        .returning('*');
    },
    async updateBookshelf(_, args) {
      console.log('==> Accessing updateBookshelf');

      return Bookshelf.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    }
  }
};

module.exports = resolvers;
