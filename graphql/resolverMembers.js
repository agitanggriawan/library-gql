const fs = require('fs');
const { Member } = require('../models');
const MyMailer = require('../lib/MyMailer');

const resolvers = {
  Query: {
    async members(_, args) {
      console.log('==> Accessing members');
      return Member.query()
        .orderBy('name', 'asc')
        .withGraphFetched('class');
    },
    async member(_, args) {
      console.log('==> Accessing member', args.id);
      return Member.query()
        .findOne({ id: args.id })
        .withGraphFetched('class');
    },
    async searchMember(_, args) {
      console.log('==> Accessing searchMember');
      const data = await Member.query()
        .where('name', 'ilike', `%${args.name}%`)
        .andWhere('status', 'Terverifikasi')
        .withGraphFetched('[class, borrow]')
        .modifyGraph('borrow', qb => {
          qb.where('is_borrowed', true);
        })
        .orderBy('name', 'asc');

      return data.filter(x => !x.borrow);
    }
  },
  Mutation: {
    async createMember(_, args) {
      console.log('==> Accessing createMember');
      let url = null;

      if (args.data.photo && args.data.photo.length) {
        url = await args.data.photo[0].then(file => {
          const { createReadStream, filename } = file;
          const fileStream = createReadStream();
          const now = Date.now();
          const path = `${now}-${filename}`;

          fileStream.pipe(fs.createWriteStream(`./public/${path}`));
          return path;
        });
      }
      args.data.photo = url;

      const result = await Member.query()
        .insert(args.data)
        .returning('*');

      if (result) {
        const sendMail = new MyMailer(
          result,
          'Pendaftaran Anggota Perpustakaan'
        );
        await sendMail.greeting();
      }

      return result;
    },
    async updateMember(_, args) {
      console.log('==> Accessing updateMember');

      return Member.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    },
    async verifyMember(_, args) {
      console.log('==> Accessing verifyMember');

      return Member.query()
        .patch({ status_token: false, status: 'Terverifikasi' })
        .where({ registration_token: args.registration_token })
        .returning('*')
        .first();
    }
  }
};

module.exports = resolvers;
