const { ApolloError } = require('apollo-server-express');
const { Officer } = require('../models');
const { generateToken } = require('../utils');

const resolvers = {
  Query: {
    async authenticate(_, args) {
      console.log('==> Accessing authenticate');
      const checkUser = await Officer.findOfficerByEmail(args.email);

      if (checkUser) {
        if (checkUser.checkPassword(args.password)) {
          const data = {
            id: checkUser.id,
            role: checkUser.role,
            name: checkUser.name,
            email: checkUser.email,
            token: 'ads'
          };

          const token = generateToken(data);
          data.token = token;
          return data;
        }
        throw new ApolloError(
          'Invalid email or password',
          'USER_NOT_FOUND',
          null
        );
      }

      throw new ApolloError(
        'Invalid email or password',
        'USER_NOT_FOUND',
        null
      );
    },
    async officers(_, args) {
      console.log('==> Accessing officers');
      return Officer.query().orderBy('name', 'asc');
    },
    async officer(_, args) {
      console.log('==> Accessing officer', args.id);
      return Officer.query().findOne({ id: args.id });
    },
    async searchOfficer(_, args) {
      console.log('==> Accessing searchOfficer');
      return Officer.query()
        .where('name', 'ilike', `%${args.name}%`)
        .orderBy('name', 'asc');
    }
  },
  Mutation: {
    async createOfficer(_, args) {
      console.log('==> Accessing createOfficer');

      return Officer.query()
        .insert(args.data)
        .returning('*');
    },
    async updateOfficer(_, args) {
      console.log('==> Accessing updateOfficer');

      return Officer.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    }
  }
};

module.exports = resolvers;
