const { merge } = require('lodash');
const { gql } = require('apollo-server-express');

/* IMPORT FOR SCHEMA */
const schemaExample = require('./schemaExample');
const schemaPublishers = require('./schemaPublishers');
const schemaOfficers = require('./schemaOfficers');
const schemaAuthors = require('./schemaAuthors');
const schemaBookshelfs = require('./schemaBookshelfs');
const schemaClasses = require('./schemaClasses');
const schemaHolidays = require('./schemaHolidays');
const schemaBooks = require('./schemaBooks');
const schemaMembers = require('./schemaMembers');
const schemaBorrows = require('./schemaBorrows');
const schemaDashboards = require('./schemaDashboards');

/* IMPORT FOR RESOLVER */
const resolverExample = require('./resolverExample');
const resolverPublishers = require('./resolverPublishers');
const resolverOfficers = require('./resolverOfficers');
const resolverAuthors = require('./resolverAuthors');
const resolverBookshelfs = require('./resolverBookshelfs');
const resolverClasses = require('./resolverClasses');
const resolverHolidays = require('./resolverHolidays');
const resolverBooks = require('./resolverBooks');
const resolverMembers = require('./resolverMembers');
const resolverBorrows = require('./resolverBorrows');
const resolverDashboards = require('./resolverDashboards');

const baseSchema = gql`
  scalar JSON
  scalar Date
  scalar DateTime
  scalar Upload

  enum Roles {
    ADMIN
    USER
  }

  type Total {
    count: Int!
  }

  type Query {
    version: String
  }

  type Mutation {
    base: String
  }
`;

const schemas = [
  baseSchema,
  schemaExample,
  schemaPublishers,
  schemaOfficers,
  schemaAuthors,
  schemaBookshelfs,
  schemaClasses,
  schemaHolidays,
  schemaBooks,
  schemaMembers,
  schemaBorrows,
  schemaDashboards,
];
const resolvers = [
  resolverExample,
  resolverPublishers,
  resolverOfficers,
  resolverAuthors,
  resolverBookshelfs,
  resolverClasses,
  resolverHolidays,
  resolverBooks,
  resolverMembers,
  resolverBorrows,
  resolverDashboards,
];

module.exports = {
  typeDefs: schemas,
  resolvers: merge(resolvers),
};
