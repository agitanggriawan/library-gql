const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    publishers: [Publisher]
    publisher(id: ID!): Publisher
    searchPublisher(name: String!): [Publisher]
  }

  extend type Mutation {
    createPublisher(data: InPublisher!): Publisher
    updatePublisher(id: ID!, data: InPublisher!): Publisher
  }

  type Publisher {
    id: ID!
    name: String!
    created_at: DateTime!
    updated_at: DateTime!
  }

  input InPublisher {
    name: String!
  }
`;

module.exports = typeDefs;
