const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    bookshelfs: [Bookshelf]
    bookshelf(id: ID!): Bookshelf
    searchBookshelf(name: String!): [Bookshelf]
  }

  extend type Mutation {
    createBookshelf(data: InBookshelf!): Bookshelf
    updateBookshelf(id: ID!, data: InBookshelf!): Bookshelf
  }

  type Bookshelf {
    id: ID!
    name: String!
    created_at: DateTime!
    updated_at: DateTime!
  }

  input InBookshelf {
    name: String!
  }
`;

module.exports = typeDefs;
