const { Holiday } = require('../models');

const resolvers = {
  Query: {
    async holidays(_, args) {
      console.log('==> Accessing holidays');
      return Holiday.query().orderBy('date_off', 'asc');
    },
    async holiday(_, args) {
      console.log('==> Accessing holiday', args.id);
      return Holiday.query().findOne({ id: args.id });
    },
  },
  Mutation: {
    async createHoliday(_, args) {
      console.log('==> Accessing createHoliday');

      return Holiday.query().insert(args.data).returning('*');
    },
    async updateHoliday(_, args) {
      console.log('==> Accessing updateHoliday');

      return Holiday.query()
        .patchAndFetchById(args.id, args.data)
        .returning('*');
    },
  },
};

module.exports = resolvers;
