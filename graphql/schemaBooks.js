const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    books: [Book]
    book(id: ID!): Book
    searchBook(title: String!): [Book]
  }

  extend type Mutation {
    createBook(data: InBook!): Book
    updateBook(id: ID!, data: InBook!): Book
  }

  type Book {
    id: ID!
    uuid: String
    publisher_id: ID!
    author_id: ID!
    bookshelf_id: ID!
    registration_number: String!
    code_number: String!
    title: String!
    quantity: Int!
    city: String!
    year: Int!
    cover_url: String
    created_at: DateTime!
    updated_at: DateTime!

    publisher: Publisher
    author: Author
    bookshelf: Bookshelf
  }

  input InBook {
    publisher_id: ID!
    author_id: ID!
    bookshelf_id: ID!
    registration_number: String!
    code_number: String!
    title: String!
    quantity: Int!
    city: String!
    year: Int!
    cover_url: Upload
  }
`;

module.exports = typeDefs;
