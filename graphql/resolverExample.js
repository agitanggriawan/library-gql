const fakeData = [
  {
    id: 1,
    url: 'https://www.imdb.com/title/tt0119177/',
    title: 'Gattaca'
  },
  {
    id: 2,
    url: 'https://www.imdb.com/title/tt0238380/',
    title: 'Equilibrium'
  },
  {
    id: 3,
    url: 'https://www.imdb.com/title/tt0118929/',
    title: 'Dark City'
  }
];

const resolvers = {
  Query: {
    movies(_, { count }) {
      // return fakeData.slice(0, count);
      // console.log('==>', casual.url);
      // return [
      //   {
      //     id: casual.day_of_week,
      //     url: casual.url,
      //     title: casual.title
      //   },
      //   {
      //     id: casual.day_of_week,
      //     url: casual.url,
      //     title: casual.title
      //   },
      //   {
      //     id: casual.day_of_week,
      //     url: casual.url,
      //     title: casual.title
      //   }
      // ];
    },
    moviesPaginate(_, { first, offset = 0 }) {
      const movies =
        first === undefined
          ? fakeData.slice(offset)
          : fakeData.slice(offset, offset + first);
      return movies;
    },
    moviesFilter(_, { filter }) {
      const where = filter
        ? {
            OR: [{ title_contains: filter }]
          }
        : {};
      console.log('==> where', where);
    },
    movie(_, args) {
      return fakeData.filter(item => args.id === item.id).pop();
    }
  },
  Mutation: {
    async createMovie(_, args) {
      console.log('==> createMovie');
      return args.data;
    },
    async createUser(_, args) {
      console.log('=>', args);
      return true;
    }
  }
};

module.exports = resolvers;
