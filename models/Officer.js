const bcrypt = require('bcrypt');
const BaseModel = require('./BaseModel');

class Officer extends BaseModel {
  static get tableName() {
    return 'officers';
  }

  static async findOfficerByEmail(email) {
    return this.query().findOne({ email });
  }

  checkPassword(password) {
    if (password) {
      const result = bcrypt.compareSync(password, this.password);
      return result;
    }
    return false;
  }
}

module.exports = Officer;
