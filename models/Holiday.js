const { ApolloError } = require('apollo-server-express');
const moment = require('moment');
const BaseModel = require('./BaseModel');

class Holiday extends BaseModel {
  static get tableName() {
    return 'holidays';
  }

  $parseDatabaseJson(json) {
    json = super.$parseDatabaseJson(json);

    if (json.date_off) {
      json.date_off = moment(json.date_off).format('YYYY-MM-DD');
    }

    return json;
  }

  async $beforeInsert(queryContext) {
    if (this.date_off) {
      const check = await Holiday.query(queryContext.transaction).where({
        date_off: moment(this.date_off).format('YYYY-MM-DD')
      });

      if (check.length) {
        throw new ApolloError('Hari libur sudah terdaftar', 'DB_ERROR', null);
      }

      return true;
    }
  }

  async $beforeUpdate(queryContext) {
    if (this.date_off) {
      const check = await Holiday.query(queryContext.transaction).where({
        date_off: moment(this.date_off).format('YYYY-MM-DD')
      });

      if (check.length) {
        throw new ApolloError('Hari libur sudah terdaftar', 'DB_ERROR', null);
      }

      return true;
    }
  }
}

module.exports = Holiday;
