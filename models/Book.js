const faker = require('faker');
const BaseModel = require('./BaseModel');

class Book extends BaseModel {
  static get tableName() {
    return 'books';
  }

  $beforeInsert() {
    this.uuid = faker.random.uuid();
  }

  static get relationMappings() {
    const Publisher = require('./Publisher');
    const Author = require('./Author');
    const Bookshelf = require('./Bookshelf');

    return {
      publisher: {
        relation: BaseModel.HasOneRelation,
        modelClass: Publisher,
        join: {
          from: 'books.publisher_id',
          to: 'publishers.id'
        }
      },
      author: {
        relation: BaseModel.HasOneRelation,
        modelClass: Author,
        join: {
          from: 'books.author_id',
          to: 'authors.id'
        }
      },
      bookshelf: {
        relation: BaseModel.HasOneRelation,
        modelClass: Bookshelf,
        join: {
          from: 'books.bookshelf_id',
          to: 'bookshelfs.id'
        }
      }
    };
  }
}

module.exports = Book;
