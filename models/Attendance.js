const BaseModel = require('./BaseModel');

class Attendance extends BaseModel {
  static get tableName() {
    return 'attendances';
  }

  static get relationMappings() {
    const Member = require('./Member');

    return {
      member: {
        relation: BaseModel.HasOneRelation,
        modelClass: Member,
        join: {
          from: 'attendances.member_id',
          to: 'members.id'
        }
      }
    };
  }
}

module.exports = Attendance;
