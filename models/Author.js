const { ApolloError } = require('apollo-server-express');
const BaseModel = require('./BaseModel');

class Author extends BaseModel {
  static get tableName() {
    return 'authors';
  }

  async $beforeInsert(queryContext) {
    if (this.name) {
      const check = await Author.query(queryContext.transaction).where({
        name: this.name
      });

      if (check.length) {
        throw new ApolloError('Nama sudah terdaftar', 'DB_ERROR', null);
      }

      return true;
    }
  }

  async $beforeUpdate(queryContext) {
    if (this.name) {
      const check = await Author.query(queryContext.transaction).where({
        name: this.name
      });

      if (check.length) {
        throw new ApolloError('Nama sudah terdaftar', 'DB_ERROR', null);
      }

      return true;
    }
  }
}

module.exports = Author;
