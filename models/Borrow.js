const moment = require('moment');
const { ref } = require('objection');
const BaseModel = require('./BaseModel');
const Holiday = require('./Holiday');
const Book = require('./Book');

class Borrow extends BaseModel {
  static get tableName() {
    return 'borrows';
  }

  $parseDatabaseJson(json) {
    json = super.$parseDatabaseJson(json);

    if (json.borrow_at) {
      json.borrow_at = moment(json.borrow_at).format('YYYY-MM-DD');
    }

    if (json.max_return) {
      json.max_return = moment(json.max_return).format('YYYY-MM-DD');
    }

    if (json.return_at) {
      json.return_at = moment(json.return_at).format('YYYY-MM-DD');
    }

    return json;
  }

  async $beforeInsert() {
    let is_borrow = false;
    this.borrow_at = moment(this.borrow_at).format('YYYY-MM-DD');
    const holidays = await Holiday.query();
    let max_return = moment(this.borrow_at).add(3, 'days');
    debugger;
    while (!is_borrow) {
      is_borrow = this.checkDate(holidays, max_return.format('YYYY-MM-DD'));

      if (is_borrow) {
        const sunday = this.checkSunday(
          moment(max_return).format('YYYY-MM-DD')
        );

        if (sunday === 0) {
          is_borrow = false;
        }
      }

      if (!is_borrow) {
        max_return = moment(max_return).add(1, 'days');
      } else {
        this.max_return = moment(max_return).format('YYYY-MM-DD');
      }
    }
  }

  $afterInsert() {
    return Book.query()
      .where({ id: this.book_id })
      .decrement('quantity', 1);
  }

  $afterUpdate() {
    return Book.query()
      .where({ id: this.book_id })
      .increment('quantity', 1);
  }

  checkDate(holidays, date) {
    const filter_holiday = holidays.filter(
      x => moment(x.date_off).format('YYYY-MM-DD') === date
    );

    if (filter_holiday.length) {
      return false;
    }

    return true;
  }

  checkSunday(date) {
    return moment(date).day();
  }

  static get relationMappings() {
    const Officer = require('./Officer');
    const Member = require('./Member');
    const Book = require('./Book');

    return {
      officer: {
        relation: BaseModel.HasOneRelation,
        modelClass: Officer,
        join: {
          from: 'borrows.officer_id',
          to: 'officers.id'
        }
      },
      member: {
        relation: BaseModel.HasOneRelation,
        modelClass: Member,
        join: {
          from: 'borrows.member_id',
          to: 'members.id'
        }
      },
      book: {
        relation: BaseModel.HasOneRelation,
        modelClass: Book,
        join: {
          from: 'borrows.book_id',
          to: 'books.id'
        }
      }
    };
  }
}

module.exports = Borrow;
