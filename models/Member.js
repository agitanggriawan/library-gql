const faker = require('faker');
const BaseModel = require('./BaseModel');

class Member extends BaseModel {
  static get tableName() {
    return 'members';
  }

  $beforeInsert() {
    this.registration_token = faker.random.uuid();
    this.status_token = true;
    this.status = 'Terdaftar';
  }

  static get relationMappings() {
    const Attendance = require('./Attendance');
    const Borrow = require('./Borrow');
    const Class = require('./Class');

    return {
      class: {
        relation: BaseModel.HasOneRelation,
        modelClass: Class,
        join: {
          from: 'members.class_id',
          to: 'classes.id'
        }
      },
      borrow: {
        relation: BaseModel.HasOneRelation,
        modelClass: Borrow,
        join: {
          from: 'members.id',
          to: 'borrows.member_id'
        }
      },
      attendances: {
        relation: BaseModel.HasManyRelation,
        modelClass: Attendance,
        join: {
          from: 'members.id',
          to: 'attendances.member_id'
        }
      }
    };
  }
}

module.exports = Member;
