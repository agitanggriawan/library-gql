const { ApolloError } = require('apollo-server-express');
const BaseModel = require('./BaseModel');

class Class extends BaseModel {
  static get tableName() {
    return 'classes';
  }

  $parseDatabaseJson(json) {
    json = super.$parseDatabaseJson(json);

    if (json.grade) {
      json.grade = `Kelas ${json.grade}`;
    }

    return json;
  }

  async $beforeInsert(queryContext) {
    if (this.grade) {
      const check = await Class.query(queryContext.transaction).where({
        grade: this.grade
      });

      if (check.length) {
        throw new ApolloError('Nama Kelas sudah terdaftar', 'DB_ERROR', null);
      }

      return true;
    }
  }

  async $beforeUpdate(queryContext) {
    if (this.grade) {
      const check = await Class.query(queryContext.transaction).where({
        grade: this.grade
      });

      if (check.length) {
        throw new ApolloError('Nama sudah terdaftar', 'DB_ERROR', null);
      }

      return true;
    }
  }
}

module.exports = Class;
