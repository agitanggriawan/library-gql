const jwt = require('jsonwebtoken');

const generateToken = data => {
  return jwt.sign(
    data,
    'dkpB3STcCuHvu02X5rILjIfvUgeipwZDNjuKS5e9UF87GYzVbrWkoePBPlHr51xAthYabItfHB3YKMRt_n6q9y97TPMz-DIGCalTBHCpO0H2a_plHga6u2ldVZ58_1xGJa5TusclpNXhzNDIUulOUHfIlKkoPO8f1V0JhsqFlFVbW8SY_BMau5X0jbqxYeu_kWVE1etL3Q92_Z3GBJibMA_O3TnEqAp0WSVlnATQiCd2LNp0Qhgp6b8S22q_sddzGNkITfk31A6t4rs72-4bfIMGiipIOA_IduTqDJbPV0RzHwa0Pvj5SkDLTCyNwGDe_h7QxQ7OKOgzQf684Adiow',
    { expiresIn: '1y' }
  );
};

const formatRegistartionNumber = number => {
  const pad = '00000';

  if (typeof number === 'undefined') return pad;
  return (pad + number).slice(-pad.length);
};

module.exports = {
  generateToken,
  formatRegistartionNumber
};
