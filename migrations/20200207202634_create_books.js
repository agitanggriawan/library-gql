exports.up = async knex => {
  await knex.schema.createTable('books', t => {
    t.increments('id').primary();
    t.string('uuid').notNullable();
    t.integer('publisher_id')
      .references('id')
      .inTable('publishers')
      .notNullable()
      .index();
    t.integer('author_id')
      .references('id')
      .inTable('authors')
      .notNullable()
      .index();
    t.integer('bookshelf_id')
      .references('id')
      .inTable('bookshelfs')
      .notNullable()
      .index();
    t.string('registration_number').notNullable(); // No Induk
    t.string('code_number').notNullable(); // Klas
    t.string('title').notNullable();
    t.integer('quantity').notNullable();
    t.string('city').notNullable();
    t.integer('year').notNullable();
    t.string('cover_url');
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('books');
};
