exports.up = async knex => {
  await knex.schema.createTable('classes', t => {
    t.increments('id').primary();
    t.string('grade').notNullable();
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('classes');
};
