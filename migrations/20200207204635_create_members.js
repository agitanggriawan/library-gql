exports.up = async knex => {
  await knex.schema.createTable('members', t => {
    t.increments('id').primary();
    t.integer('class_id')
      .references('id')
      .inTable('classes')
      .notNullable()
      .index();
    t.string('name').notNullable();
    t.string('email');
    t.text('address');
    t.enu('gender', ['Laki-laki', 'Perempuan']);
    t.string('status');
    t.string('phone');
    t.date('birthdate');
    t.string('photo');
    t.string('registration_token');
    t.boolean('status_token');
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('members');
};
