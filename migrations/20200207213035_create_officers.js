exports.up = async knex => {
  await knex.schema.createTable('officers', t => {
    t.increments('id').primary();
    t.string('role').notNullable();
    t.string('email').notNullable();
    t.string('name').notNullable();
    t.text('address');
    t.string('phone');
    t.string('password');
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('officers');
};
