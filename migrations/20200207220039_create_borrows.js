exports.up = async knex => {
  await knex.schema.createTable('borrows', t => {
    t.increments('id').primary();
    t.integer('officer_id')
      .references('id')
      .inTable('officers')
      .notNullable()
      .index();
    t.integer('member_id')
      .references('id')
      .inTable('members')
      .notNullable()
      .index();
    t.integer('book_id')
      .references('id')
      .inTable('books')
      .notNullable()
      .index();
    t.boolean('is_borrowed').default(true);
    t.date('borrow_at');
    t.date('max_return');
    t.date('return_at');
    t.integer('penalty');
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('borrows');
};
