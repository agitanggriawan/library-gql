exports.up = async knex => {
  await knex.schema.createTable('attendances', t => {
    t.increments('id').primary();
    t.integer('member_id')
      .references('id')
      .inTable('members')
      .notNullable()
      .index();
    t.string('activity');
    t.date('attend_at');
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('attendances');
};
