exports.up = async knex => {
  await knex.schema.createTable('holidays', t => {
    t.increments('id').primary();
    t.date('date_off').notNullable();
    t.string('name').notNullable();
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('holidays');
};
