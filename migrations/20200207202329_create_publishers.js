exports.up = async knex => {
  await knex.schema.createTable('publishers', t => {
    t.increments('id').primary();
    t.string('name').notNullable();
    t.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('publishers');
};
